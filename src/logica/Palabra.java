package logica;

import java.util.ArrayList;

public class Palabra {
	private ArrayList<String> palabras = new ArrayList<String>();

	public ArrayList<String> getPalabras() {
		return palabras;
	}

	public void setPalabras(ArrayList<String> palabras) {
		this.palabras = palabras;
	}

	public String dejarRaices() {

		String salida = "";
		for (int i = 0; i < palabras.size(); i++) {
			salida += devolverRaiz(palabras.get(i)) + " \n";
		}
		return salida;
	}

	public String devolverRaiz(String palabra) {
		String raiz;
		org.tartarus.snowball.ext.EnglishStemmer miE = new org.tartarus.snowball.ext.EnglishStemmer();
		miE.setCurrent(palabra);
		miE.stem();
		raiz = (miE.getCurrent());
		return raiz;
	}

}
