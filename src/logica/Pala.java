package logica;

public class Pala {
	
	String palabra;
	int cantidad;
	
	@Override
	public String toString() {
		return " [palabra=" + palabra + ", cantidad=" + cantidad + "]";
	}
	
	public Pala(String palabra, int cantidad) {
				this.palabra = palabra;
		this.cantidad = cantidad;
	}
	public String getPalabra() {
		return palabra;
	}
	public void setPalabra(String palabra) {
		this.palabra = palabra;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
}
