package logica;

public class Resultado {
	private String Ontologia;
	private int numeroApariciones;
	private String documento;
	private int cantPalDoc;
	
	public Resultado(String ontologia, int numeroApariciones, String documento, int cantPalDoc) {
		Ontologia = ontologia;
		this.numeroApariciones = numeroApariciones;
		this.documento = documento;
		this.cantPalDoc = cantPalDoc;
	}
	
	
	
	@Override
	public String toString() {
		return "Resultado [Ontologia=" + Ontologia + ", numeroApariciones=" + numeroApariciones + ", documento="
				+ documento + ", cantPalDoc=" + cantPalDoc + "]";
	}



	public String getOntologia() {
		return Ontologia;
	}
	public void setOntologia(String ontologia) {
		Ontologia = ontologia;
	}
	public int getNumeroApariciones() {
		return numeroApariciones;
	}
	public void setNumeroApariciones(int numeroApariciones) {
		this.numeroApariciones = numeroApariciones;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public int getCantPalDoc() {
		return cantPalDoc;
	}
	public void setCantPalDoc(int cantPalDoc) {
		this.cantPalDoc = cantPalDoc;
	}
	
}
