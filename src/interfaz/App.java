package interfaz;


import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import logica.Analizador;
import logica.Pala;
import logica.Resultado;
import logica.Termino;

public class App {

	public static void main(String[] args) {
		
		File f = new File("./src/cl");
		File[] ficheros = f.listFiles();
		ArrayList<ArrayList<Termino>> misTerminos = new ArrayList<ArrayList<Termino>>();
		HashMap<String, Integer> vectorGeneral = new HashMap<String, Integer>();
		ArrayList<Resultado> resultados = new ArrayList<Resultado>();
		for (int x=10;x<11;x++){
			String nomDoc = ficheros[x].getName();
			File archivo = new File ("./src/cl/"+nomDoc);
			
			FileReader fr = null;
			try {
				fr = new FileReader(archivo);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			BufferedReader br = new BufferedReader(fr);
			String linea="";
			String texto = "";
			try {
				while((linea=br.readLine())!=null)
					
				    texto+=linea;
			} catch (IOException e) {
				e.printStackTrace();
			}
			Analizador a = new Analizador(texto);
			a.analizar();
		//System.out.println(a.getListaPalabras());
//			misTerminos.add(a.getlistaTerminos());
						
			HashMap<String,Integer> misPalabras = a.getListaPalabras();
			
			vectorGeneral.putAll(misPalabras);
			System.out.println("Documentos "+ficheros[x].getName()+
					"\n Terminos con su codigo " + a.getlistaTerminos()+
					"\n Palabras con su cantidad " +misPalabras.size() +" "+ misPalabras+
					"\n Cantidad de palabras doc " + a.cantPalDoc);
			
			String query = "cell";
			query = query.toLowerCase();
			String[] ps = query.split(" ");
			
			if(ps.length>1) {
				query = "";
				for (int i = 0; i < ps.length; i++) {
					String aux = ps[i];
					query += devolverRaiz(aux);
				}
			}else {
				query = devolverRaiz(query);
			}
			
			if (misPalabras.containsKey(query)) {
				int cantidad = misPalabras.get(query);
				Resultado r = new Resultado(query, cantidad, nomDoc, a.cantPalDoc);
				resultados.add(r);
			}
		}
		
		Resultado[] rts = new Resultado[resultados.size()];
		
		int cont = 0;
		while (!resultados.isEmpty()) {
			Resultado mayor = resultados.get(0);
			
			for (int i = 0; i < resultados.size(); i++) {
				if(mayor.getNumeroApariciones()<resultados.get(i).getNumeroApariciones()) {
					mayor = resultados.get(i);
				}
			}
			
			rts[cont] = mayor;
			cont++;
			resultados.remove(mayor);
		}
		
		for (int i = 0; i < rts.length; i++) {
			System.err.println(rts[i]);	
		}
		
			
		
	}

	public static String devolverRaiz(String palabra) {
		String raiz;
		org.tartarus.snowball.ext.EnglishStemmer miE = new org.tartarus.snowball.ext.EnglishStemmer();
		miE.setCurrent(palabra);
		miE.stem();
		raiz = (miE.getCurrent());
		return raiz;
	}
	private static boolean estaPalabra(String palbusqueda, ArrayList<Pala> misP) {
		for (int i = 0; i < misP.size(); i++) {
			if(misP.get(i).getPalabra().equals(palbusqueda)) {
				misP.get(i).setCantidad(misP.get(i).getCantidad()+1);
				return true;
			}
		}
		return false;
	}


	

}
