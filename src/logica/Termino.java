package logica;

public class Termino {

	private String codigo;
	private String palabra;
	private int cont;

	public Termino() {
		this.palabra = "";
		this.codigo = "";
	}
	
	
	@Override
	public String toString() {
		return "Termino {codigo=" + codigo + ", palabra=" + palabra + "}";
	}


	public Termino(String codigo,String palabra) {
		this.palabra = palabra;
		this.codigo = codigo;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getPalabra() {
		return palabra;
	}


	public void setPalabra(String palabra) {
		this.palabra = palabra;
	}
	
	
	
	
	
	
	
}
