package logica;

import java.util.ArrayList;
import java.util.HashMap;

public class Analizador {
	
	private String codigoFuente;
	private ArrayList<Termino> listaTerminos;
	private HashMap<String,Integer> listaPalabras;
	public int cantPalDoc;
	private int posActual, filaActual, columnaActual;
	private char caracterActual, finCodigo;
	
	public Analizador(String codigoFuente) {
		this.codigoFuente = codigoFuente;
		this.listaPalabras = new HashMap();
		this.cantPalDoc = 0;
		this.listaTerminos = new ArrayList<Termino>();
		this.caracterActual = codigoFuente.charAt(0);
		this.finCodigo = 0;	
	}
	
	public void analizar() {
		
		while( caracterActual!=finCodigo ) {
			
			if( caracterActual=='\t' || caracterActual==' ' || caracterActual=='\n' ) {
				obtenerSiguienteCaracter();
				continue;
			}
			
			if(esTermino()) continue;
			
			
			
			obtenerSiguienteCaracter();
		}	
	}
	
	private boolean esPalabra() {
//		System.out.println(caracterActual);
		if (caracterActual > 96 && caracterActual < 123 || caracterActual > 64 && caracterActual < 91 ) {
			cantPalDoc++;
			
			while (caracterActual!=' ' && posActual < codigoFuente.length()) {
				obtenerSiguienteCaracter();
			}
			return true;
		}else {
			return false;
			
		}
		
	}

	private boolean esTermino() {
		
		if(caracterActual == '<') {
			
			obtenerSiguienteCaracter();
			if(caracterActual == 't') {
				
				obtenerSiguienteCaracter();
				if (caracterActual == 'e') {
					obtenerSiguienteCaracter();
					if(caracterActual == 'r') {
						obtenerSiguienteCaracter();
						if (caracterActual == 'm') {
							Termino t = new Termino();
							obtenerSiguienteCaracter();
							obtenerSiguienteCaracter();
							obtenerSiguienteCaracter();
							obtenerSiguienteCaracter();
							obtenerSiguienteCaracter();
							obtenerSiguienteCaracter();
							obtenerSiguienteCaracter();
							String cod="";
							
							while (caracterActual!='"') {
								cod+=caracterActual+"";
								obtenerSiguienteCaracter();		
							}
							obtenerSiguienteCaracter();
							String palabra = "";
							while (caracterActual!='<') {
								obtenerSiguienteCaracter();
								if(caracterActual!='<') {
									palabra+=caracterActual;	
								}else {
									String[] ps = palabra.split(" ");
									
									if(ps.length>1) {
										palabra = "";
										for (int i = 0; i < ps.length; i++) {
											String aux = ps[i];
											palabra += devolverRaiz(aux);
										}
									}else {
										palabra = devolverRaiz(palabra);
									}
									palabra = palabra.toLowerCase();
									if (listaPalabras.containsKey(palabra)) {
										cantPalDoc++;
										listaPalabras.replace(palabra, listaPalabras.get(palabra)+1);
									}else {
										cantPalDoc++;
										listaPalabras.put(palabra, 1);	
									}
								}
							}
							
							
							t.setCodigo(cod);
							t.setPalabra(palabra);
							listaTerminos.add(t);
						}
					}
				}
			}
		}
		return false;
	}

	
	public String devolverRaiz(String palabra) {
		String raiz;
		org.tartarus.snowball.ext.EnglishStemmer miE = new org.tartarus.snowball.ext.EnglishStemmer();
		miE.setCurrent(palabra);
		miE.stem();
		raiz = (miE.getCurrent());
		return raiz;
	}
	

	public HashMap<String,Integer> getListaPalabras() {
		return listaPalabras;
	}

	public void setListaPalabras(HashMap<String,Integer> listaPalabras) {
		this.listaPalabras = listaPalabras;
	}

	public void obtenerSiguienteCaracter() {
		
		posActual++;
		
		if(posActual < codigoFuente.length()) {
			
			if(caracterActual == '\n') {
				filaActual++;
				columnaActual = 0;
			}else {
				columnaActual++;
			}
			
			caracterActual = codigoFuente.charAt(posActual);
			
		}else {
			caracterActual = finCodigo;
		}
		
	}
	
	public void hacerBT(int posInicial, int fila, int columna) {
		posActual = posInicial;
		filaActual = fila;
		columnaActual = columna;
		caracterActual = codigoFuente.charAt(posInicial);		
	}

	public ArrayList<Termino> getlistaTerminos() {
		return listaTerminos;
	}

	public void setlistaTerminos(ArrayList<Termino> listaTerminos) {
		this.listaTerminos = listaTerminos;
	}
	
	
	public void agregarTermino()
	{
		
		
	}
	

}
