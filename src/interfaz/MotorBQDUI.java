package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;

public class MotorBQDUI {

	private JFrame frmBqdBrowser;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MotorBQDUI window = new MotorBQDUI();
					window.frmBqdBrowser.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MotorBQDUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmBqdBrowser = new JFrame();
		frmBqdBrowser.setTitle("BQD Browser");
		frmBqdBrowser.setBounds(100, 100, 615, 341);
		frmBqdBrowser.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBqdBrowser.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(46, 124, 526, 33);
		frmBqdBrowser.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnBuscar = new JButton("Buscar en BQD");
		btnBuscar.setBounds(118, 193, 136, 23);
		frmBqdBrowser.getContentPane().add(btnBuscar);
		
		JButton btnSuerte = new JButton("Me siento con suerte");
		btnSuerte.setBounds(340, 193, 153, 23);
		frmBqdBrowser.getContentPane().add(btnSuerte);
		
		JLabel lblOfrecidoPorBqd = new JLabel("Ofrecido por BQD en: Espa\u00F1ol (Latinoam\u00E9rica)");
		lblOfrecidoPorBqd.setBounds(194, 247, 237, 14);
		frmBqdBrowser.getContentPane().add(lblOfrecidoPorBqd);
		
		JLabel lblBqd = new JLabel("BQD");
		lblBqd.setFont(new Font("Palatino Linotype", Font.ITALIC, 76));
		lblBqd.setBounds(222, 11, 183, 114);
		frmBqdBrowser.getContentPane().add(lblBqd);
	}
}
